#include "maincharacter.h"

MainCharacter::MainCharacter(CSDL_Setup* passed_SDL_Setup,
                             int* passed_MouseX, int* passed_MouseY, float *passed_CameraX, float *passed_CameraY, Env passed_env)
{
    CameraX = passed_CameraX;
    CameraY = passed_CameraY;
    csdl_setup = passed_SDL_Setup;
    MouseX = passed_MouseX;
    MouseY = passed_MouseY;
    bob = new Sprite(csdl_setup->GetRenderer(),
                     "/home/stas/qt-creator/projects/build-sdlgame-Desktop-Default/res/sdlgame/tom.png",
                     300, 250, 100, 120, CameraX, CameraY, CollisionRectangle(250, 130, 100, 120));
    bob->SetUpAnimation(4, 4);

    bob->SetOrgin(bob->GetWidth()/2.0f, bob->GetHeight());

    timeCheck = SDL_GetTicks();
    Follow = false;
    distance = 0;
    stopAnimation = false;
}

MainCharacter::~MainCharacter()
{
    delete bob;
}

void MainCharacter::UpdateAnimation()
{
    float angle = atan2(FollowPointY - *CameraY,
                        FollowPointX - *CameraX);
    angle = (angle * (180/3.14)) + 180;
    if (!stopAnimation){

        if (angle > 45 && angle <= 135)
        {
            if (distance > 3)
                bob->PlayAnimation(0, 3, 0, 200);
            else
                bob->PlayAnimation(1, 1, 0, 200);
        }
        else if (angle > 135 && angle < 225)
        {
            if (distance > 3)
                bob->PlayAnimation(0, 3, 1, 200);
            else
                bob->PlayAnimation(1, 1, 1, 200);
        }
        else if (angle > 225 && angle < 315)
        {
            if (distance > 3)
                bob->PlayAnimation(0, 3, 3, 200);
            else
                bob->PlayAnimation(1, 1, 3, 200);
        }
        else if ((angle <= 360 && angle > 315) || (angle >=0 && angle <= 45))
        {
            if (distance > 3)
                bob->PlayAnimation(0, 3, 2, 200);
            else
                bob->PlayAnimation(1, 1, 2, 200);
        }
    }
}

void MainCharacter::UpdateControls()
{
    if (csdl_setup->GetMainEvent()->type == SDL_MOUSEBUTTONDOWN || csdl_setup->GetMainEvent()->type == SDL_MOUSEMOTION)
    {
        if (csdl_setup->GetMainEvent()->button.button == SDL_BUTTON_LEFT)
        {
            FollowPointX = *CameraX - *MouseX + 300;
            FollowPointY = *CameraY - *MouseY + 250;
            Follow = true;
        }
    }

    if ((timeCheck + 10) < SDL_GetTicks() && Follow)
    {
        distance = GetDistance(*CameraX, *CameraY, FollowPointX, FollowPointY);
        if (distance != 0)
        {
            bool colliding = false;
            for (int i = 0; i < env.GetTrees().size(); i++)
            {
                if (bob->isColliding(env.GetTrees()[i]->GetTrunk()->GetCollisionRect()))
                {
                    if (*CameraX > FollowPointX)
                    {
                        *CameraX = *CameraX + 5;
                    }
                    if (*CameraX < FollowPointX)
                    {
                        *CameraX = *CameraX - 5;
                    }

                    if (*CameraY > FollowPointY)
                    {
                        *CameraY = *CameraY + 5;
                    }
                    if (*CameraY < FollowPointY)
                    {
                        *CameraY = *CameraY - 5;
                    }
                    FollowPointX = *CameraX;
                    FollowPointY = *CameraY;
                    distance = 0;
                    Follow = false;

                    colliding = true;
                }
            }

            if (!colliding)
            {
                if (*CameraX != FollowPointX)
                {
                    *CameraX = *CameraX - ((*CameraX - FollowPointX)/distance) * 1.5f;
                }

                if (*CameraY != FollowPointY)
                {
                    *CameraY = *CameraY - ((*CameraY - FollowPointY)/distance) * 1.5f;
                }
            }
        }
        else
        {
            Follow = false;
        }
        timeCheck = SDL_GetTicks();
    }
}

double MainCharacter::GetDistance(int X1, int Y1, int X2, int Y2)
{
    double DifferenceX = X1 - X2;
    double DifferenceY = Y1 - Y2;
    double distance = sqrt((DifferenceX * DifferenceX) + (DifferenceY * DifferenceY));
    return distance;
}

void MainCharacter::Draw()
{
    bob->DrawSteady();
}

void MainCharacter::Update()
{
    UpdateAnimation();
    UpdateControls();
}
