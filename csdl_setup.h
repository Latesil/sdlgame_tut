#ifndef CSDL_SETUP_H
#define CSDL_SETUP_H

#include "main_headers.h"

class CSDL_Setup
{
public:
    CSDL_Setup(bool* quit, int ScreenWidth, int Screenheight);
    ~CSDL_Setup();
    SDL_Renderer* GetRenderer();
    SDL_Event* GetMainEvent();

    void Begin();
    void End();

private:
    SDL_Window* window;
    SDL_Renderer* renderer;
    SDL_Event* mainEvent;
};

#endif // CSDL_SETUP_H
