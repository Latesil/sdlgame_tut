#ifndef ENV_H
#define ENV_H

#include "main_headers.h"
#include "sprite.h"
#include "csdl_setup.h"
#include "tree.h"
#include <vector>
#include <fstream>
#include <sstream>
#include <string>

class Env
{
private:
    Sprite* grass[4][7];
    std::vector<Tree*> trees;
    CSDL_Setup* sdl_setup;
    float* CameraX;
    float* CameraY;
    bool OnePressed;
    int Mode;

public:
    Env(int ScreenWidth, int ScreenHeight, float *passed_CameraX, float *passed_CameraY, CSDL_Setup *csdl_setup);
    ~Env();
    std::vector<Tree*> GetTrees() { return trees; }
    void DrawBack();
    void DrawFront();
    void SaveToFile();
    void LoadFromFile();
    enum ModeType
    {
        GamePlay,
        LevelCreation,
    };
    void Update();
};

#endif // ENV_H
