#include "cmainclass.h"

using namespace std;

int main(int, char**)
{
    CMainClass* cmainclass = new CMainClass(700, 300);

    cmainclass->GameLoop();

    delete cmainclass;

    return 0;
}
