#ifndef SPRITE_H
#define SPRITE_H

#include "main_headers.h"
#include "collisionrectangle.h"

class Sprite
{
public:
    Sprite(SDL_Renderer *passed_renderer, std::string FilePath, int x,
           int y, int w, int h, float* passed_CameraX, float* passed_CameraY,
           CollisionRectangle passed_CollisionRect);
    ~Sprite();
    void Draw();
    void DrawSteady();
    void SetX(float X);
    void SetY(float Y);
    void SetPosition(float X, float Y);
    void SetOrgin(float X, float Y);
    void PlayAnimation(int BeginFrame, int EndFrame, int Row, float Speed);
    void SetUpAnimation(int passed_Amount_X, int passed_Amount_Y);

    int GetWidth();
    int GetHeight();
    void SetWidth(int W);
    void SetHeight(int H);
    bool isColliding(CollisionRectangle theCOllider);
    CollisionRectangle GetCollisionRect() { return CollisionRect; }
    float GetX();
    float GetY();

private:
    CollisionRectangle CollisionRect;
    SDL_Rect collisionSDLRect;
    SDL_Rect Camera;
    float Orgin_X;
    float Orgin_Y;
    float X_pos;
    float Y_pos;

    int img_width;
    int img_height;
    int CurrentFrame;
    int animationDelay;
    int Amount_Frame_X;
    int Amount_Frame_Y;
    float* CameraX;
    float* CameraY;

    SDL_Texture* image;
    SDL_Texture* CollisionImage;
    SDL_Rect rect;
    SDL_Rect crop;
    SDL_Renderer* renderer;
};

#endif // SPRITE_H
