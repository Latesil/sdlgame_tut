#ifndef TREE_H
#define TREE_H

#include "main_headers.h"
#include "sprite.h"
#include "csdl_setup.h"

class Tree
{
public:
    Tree(int x, int y, float* CameraX, float* CameraY, CSDL_Setup* csdl_setup);
    ~Tree();
    Sprite* GetCrown() {return Crown;}
    Sprite* GetTrunk() {return Trunk;}

    void DrawCrown();
    void DrawTrunk();
    int GetX();
    int GetY();
private:
    Sprite* Crown;
    Sprite* Trunk;
    int x, y;
};

#endif // TREE_H
