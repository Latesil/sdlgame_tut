#ifndef CMAINCLASS_H
#define CMAINCLASS_H


#include "main_headers.h"
#include "csdl_setup.h"
#include "sprite.h"
#include <math.h>
#include "maincharacter.h"
#include "env.h"

class CMainClass
{
public:
    CMainClass(int passed_ScreenWidth, int passed_ScreenHeight);
    ~CMainClass();
    void GameLoop();

private:
    float CameraX;
    float CameraY;
    MainCharacter* bob;
    Env* ForestArea;

    int ScreenWidth;
    int ScreenHeight;
    int MouseX;
    int MouseY;

    bool quit;

    CSDL_Setup* csdl_setup;
};

#endif // CMAINCLASS_H
