#ifndef COLLISIONRECTANGLE_H
#define COLLISIONRECTANGLE_H

#include "main_headers.h"


class CollisionRectangle
{
public:
    CollisionRectangle();
    CollisionRectangle(int x, int y, int w, int h);
    ~CollisionRectangle();

    void SetRectangle(int x, int y, int w, int h);
    SDL_Rect GetRectangle() const { return CollisionRect; }
    void SetX(int x){ CollisionRect.x = x + OffsetX;}
    void SetY(int y){ CollisionRect.y = y + OffsetY;}

private:
    int OffsetX;
    int OffsetY;
    SDL_Rect CollisionRect;
};

#endif // COLLISIONRECTANGLE_H
