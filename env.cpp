#include "env.h"

Env::Env(int ScreenWidth, int ScreenHeight, float *passed_CameraX,
         float *passed_CameraY, CSDL_Setup* csdl_setup)
{
    sdl_setup = csdl_setup;
    CameraX = passed_CameraX;
    CameraY = passed_CameraY;

    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 7; j++)
        {
            grass[i][j] = new Sprite(csdl_setup->GetRenderer(),
                               "/home/stas/qt-creator/projects/build-sdlgame-Desktop-Default/res/sdlgame/env/grass.bmp",
                               ScreenWidth * i, ScreenHeight * j, ScreenWidth, ScreenHeight, CameraX, CameraY, CollisionRectangle(0, 0, 100, 120));
        }
    }
    Mode = GamePlay;
    LoadFromFile();
    OnePressed = false;


//    tree = new Tree(300, 300, CameraX, CameraY, csdl_setup);
}

Env::~Env()
{
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 7; j++)
        {
            delete grass[i][j];
        }
    }
    for (std::vector<Tree*>::iterator i = trees.begin(); i != trees.end(); ++i)
    {
        delete (*i);
    }
    trees.clear();
}

void Env::DrawBack()
{
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 7; j++)
        {
            grass[i][j]->Draw();
        }
    }
    for (std::vector<Tree*>::iterator i = trees.begin(); i != trees.end(); ++i)
    {
        (*i)->DrawTrunk();
    }
}

void Env::DrawFront()
{
    for (std::vector<Tree*>::iterator i = trees.begin(); i != trees.end(); ++i)
    {
        (*i)->DrawCrown();
    }
}

void Env::LoadFromFile()
{
    std::ifstream LoadedFile ("/home/stas/qt-creator/projects/build-sdlgame-Desktop-Default/res/sdlgame/env/StageLayout.txt");

    std::string line;

    enum ObjectType{
        TypeNone,
        TypeTree
    };

    int CurrentType = TypeNone;

    if (LoadedFile.is_open())
    {
        while ( LoadedFile.good() )
        {
            std::getline(LoadedFile, line);
            if (line == "---===Begin_Tree===---")
            {
                CurrentType = TypeTree;
            }
            else if (line == "---===End_Tree===---")
            {
                CurrentType = TypeNone;
            }
            else
            {
                if (CurrentType == TypeTree)
                {

                    std::istringstream iss(line);

                    int TempX = 0;
                    int TempY = 0;
                    std::string PreviousWord = "";

                    while (iss)
                    {
                        std::string word;
                        iss >> word;

                        if (PreviousWord == "x:")
                        {
                            TempX = atoi( word.c_str() );
                        }

                        if (PreviousWord == "y:")
                        {
                            TempY = atoi( word.c_str() );

                            trees.push_back(new Tree(TempX, TempY,CameraX, CameraY, sdl_setup));
                        }

                        PreviousWord = word;
                    }


                }
            }
        }
    }
    else
    {
        std::cout << "File Could Not Be Open: StageLayout.txt" << std::endl;
    }
}

void Env::SaveToFile()
{
    std::ofstream LoadedFile;
    LoadedFile.open("/home/stas/qt-creator/projects/build-sdlgame-Desktop-Default/res/sdlgame/env/StageLayout.txt");

    LoadedFile << "---===Begin_Tree===---" << std::endl;

    for (std::vector<Tree*>::iterator i = trees.begin(); i != trees.end(); ++i)
    {
        LoadedFile << "x: " << (*i)->GetX() << "\ty: " << (*i)->GetY() << std::endl;
    }

    LoadedFile << "---===End_Tree===---" << std::endl;

    LoadedFile.close();

    std::cout << "Level Saved!" << std::endl;
}

void Env::Update()
{
    if (Mode == LevelCreation)
        {

            if (sdl_setup->GetMainEvent()->type == SDL_KEYDOWN)
            {
                if (!OnePressed && sdl_setup->GetMainEvent()->key.keysym.sym == SDLK_F11)
                {
                    SaveToFile();
                    OnePressed = true;
                }
            }

            if (sdl_setup->GetMainEvent()->type == SDL_KEYUP)
            {
                if (OnePressed && sdl_setup->GetMainEvent()->key.keysym.sym == SDLK_F11)
                {
                    OnePressed = false;
                }
            }



            if (sdl_setup->GetMainEvent()->type == SDL_KEYDOWN)
            {
                if (!OnePressed && sdl_setup->GetMainEvent()->key.keysym.sym == SDLK_F1)
                {
                    trees.push_back(new Tree(-*CameraX+ 275,-*CameraY + 90,CameraX, CameraY, sdl_setup));
                    OnePressed = true;
                }
            }

            if (sdl_setup->GetMainEvent()->type == SDL_KEYUP)
            {
                if (OnePressed && sdl_setup->GetMainEvent()->key.keysym.sym == SDLK_F1)
                {
                    OnePressed = false;
                }
            }




            if (sdl_setup->GetMainEvent()->type == SDL_KEYDOWN)
            {
                if (!OnePressed && sdl_setup->GetMainEvent()->key.keysym.sym == SDLK_F2)
                {
                    if (trees.size() > 0)
                    {
                        int count = 0;

                        for (std::vector<Tree*>::iterator i = trees.begin(); i != trees.end(); ++i)
                        {
                            if (count == trees.size())
                                delete (*i);

                            count++;
                        }

                        trees.pop_back();

                    }
                    OnePressed = true;
                }
            }

            if (sdl_setup->GetMainEvent()->type == SDL_KEYUP)
            {
                if (OnePressed && sdl_setup->GetMainEvent()->key.keysym.sym == SDLK_F2)
                {
                    OnePressed = false;
                }
            }
        }


        if (sdl_setup->GetMainEvent()->type == SDL_KEYDOWN)
        {
            if (!OnePressed && sdl_setup->GetMainEvent()->key.keysym.sym == SDLK_F12)
            {
                if (Mode == LevelCreation)
                {
                    std::cout << "Level Creation: OFF" << std::endl;
                    Mode = GamePlay;
                }
                else if (Mode == GamePlay)
                {
                    std::cout << "Level Creation: ON" << std::endl;
                    Mode = LevelCreation;
                }

                OnePressed = true;
            }
        }

        if (sdl_setup->GetMainEvent()->type == SDL_KEYUP)
        {
            if (OnePressed && sdl_setup->GetMainEvent()->key.keysym.sym == SDLK_F12)
            {
                OnePressed = false;
            }
        }
}
