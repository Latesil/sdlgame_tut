#include "csdl_setup.h"

CSDL_Setup::CSDL_Setup(bool *quit, int ScreenWidth, int Screenheight)
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0){
            std::cout << "Can't init" << std::endl;
            return;
    }

    window = nullptr;
    window = SDL_CreateWindow("Lesson 3", 100, 100, ScreenWidth, Screenheight, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (window == nullptr){
        *quit = true;
    }

    renderer = nullptr;
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == nullptr){
        std::cout << "can't render" << std::endl;
        SDL_DestroyWindow(window);
        SDL_Quit();
        return;
    }

    mainEvent = new SDL_Event();
}

CSDL_Setup::~CSDL_Setup()
{
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    delete mainEvent;
}

SDL_Renderer* CSDL_Setup::GetRenderer()
{
    return renderer;
}

SDL_Event* CSDL_Setup::GetMainEvent()
{
    return mainEvent;
}

void CSDL_Setup::Begin()
{
    SDL_PollEvent(mainEvent);
    SDL_RenderClear(renderer);
}

void CSDL_Setup::End()
{
    SDL_RenderPresent(renderer);
}
