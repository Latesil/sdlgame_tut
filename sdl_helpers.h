#ifndef SDL_HELPERS_H
#define SDL_HELPERS_H

#include "main_headers.h"

std::string getResourcePath(const std::string &subDir);
void logSDLError(std::ostream &os, const std::string &msg);
SDL_Texture* loadTexture(const std::string &file, SDL_Renderer* ren);


#endif // SDL_HELPERS_H
