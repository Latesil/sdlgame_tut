#ifndef MAINCHARACTER_H
#define MAINCHARACTER_H

#include "main_headers.h"
#include "sprite.h"
#include "csdl_setup.h"
#include <math.h>
#include "env.h"

class MainCharacter
{
public:
    MainCharacter(CSDL_Setup* passed_SDL_Setup,
                  int* passed_MouseX, int* passed_MouseY, float* CameraX, float* passed_CameraY, Env passed_env);
    ~MainCharacter();
    double GetDistance(int X1, int Y1, int X2, int Y2);
    void Update();
    void Draw();
private:
    Env env;
    void UpdateAnimation();
    void UpdateControls();
    Sprite* bob;
    int timeCheck;
    float *CameraX;
    float *CameraY;
    int *MouseX;
    int *MouseY;

    CSDL_Setup* csdl_setup;

    int FollowPointX;
    int FollowPointY;
    bool Follow;
    float distance;
    bool stopAnimation;
};

#endif // MAINCHARACTER_H
