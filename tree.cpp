#include "tree.h"

Tree::Tree(int x_passed, int y_passed, float *CameraX, float *CameraY, CSDL_Setup *csdl_setup)
{
    x = x_passed;
    y = y_passed;
    Trunk = new Sprite(csdl_setup->GetRenderer(),
                       "/home/stas/qt-creator/projects/build-sdlgame-Desktop-Default/res/sdlgame/env/trunk.png",
                       x, y, 43, 145, CameraX, CameraY, CollisionRectangle(0, 0, 43, 145) );
    Crown = new Sprite(csdl_setup->GetRenderer(),
                       "/home/stas/qt-creator/projects/build-sdlgame-Desktop-Default/res/sdlgame/env/Crown.png",
                       x-75, y-115, 183, 165, CameraX, CameraY, CollisionRectangle(0, 0, 183, 165));
}

Tree::~Tree()
{
    delete Trunk;
    delete Crown;
}

int Tree::GetX()
{
    return x;
}

int Tree::GetY()
{
    return y;
}

void Tree::DrawCrown()
{
    Crown->Draw();
}

void Tree::DrawTrunk()
{
    Trunk->Draw();
}
